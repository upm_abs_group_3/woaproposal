package es.upm.emse.absd.ontology.woa;

public class WoaOntologyVocabulary {
    public static final String ONTOLOGY_NAME = "WoaOntology";
    //actions
    public static final String INFORM_INITIAL_RESOURCES = "informInitialResources";
    public static final String REGISTER = "register";
    public static final String CHANGE_PHASE = "changePhase";
    public static final String ASSIGN_NEW_UNIT = "assignNewUnit";
    public static final String ALLOCATE_UNIT = "allocateUnit";
    public static final String DISTRIBUTE_MAP = "distributeMap";
    //concepts
    public static final String RESOURCE = "resource";
    public static final String RESOURCE_TYPE = "typeRes";
    public static final String RESOURCE_AMOUNT = "amount";

    public static final String INITIAL_RESOURCES = "initialResources";
    public static final String RESOURCE_GOLD = "gold";
    public static final String RESOURCE_STONE = "stone";
    public static final String RESOURCE_WOOD = "wood";
    public static final String TRIBE_RESOURCES = "tribeResources";
    public static final String STORAGE = "storage";

    public static final String STORAGE_CAPACITY = "storageCapacity";
    public static final String STORAGE_SIZE = "size";

    public static final String NEW_PHASE = "newPhase";
    public static final String PHASE_ID = "phase";

    public static final String COORDINATE = "coordinate";
    public static final String COORDINATE_XVALUE = "xValue";
    public static final String COORDINATE_YVALUE = "yValue";
    public static final String COORDINATE_INITIAL_POSITION = "initialPosition";
    public static final String COORDINATE_MAP_SIZE = "mapSize";

    public static final String AID_UNIT_ID = "unitID";

    private WoaOntologyVocabulary() {}
}
