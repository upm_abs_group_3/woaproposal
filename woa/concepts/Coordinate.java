package es.upm.emse.absd.ontology.woa.concepts;

import jade.content.Concept;
import lombok.Data;

@Data
public class Coordinate implements Concept {
    private int xValue;
    private int yValue;
}