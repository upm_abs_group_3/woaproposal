package es.upm.emse.absd.ontology.woa.concepts;

import jade.content.Concept;
import lombok.Data;

@Data
public class Resource implements Concept {

    private String typeRes;
    private float amount;
}
