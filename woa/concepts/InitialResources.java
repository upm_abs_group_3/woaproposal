package es.upm.emse.absd.ontology.woa.concepts;

import jade.content.Concept;
import lombok.Data;

@Data
public class InitialResources implements Concept {

    private Resource gold;
    private Resource stone;
    private Resource wood;
}
