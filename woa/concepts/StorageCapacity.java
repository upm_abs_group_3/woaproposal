package es.upm.emse.absd.ontology.woa.concepts;

import jade.content.Concept;
import lombok.Data;

@Data
public class StorageCapacity implements Concept {
    private float size;
}
