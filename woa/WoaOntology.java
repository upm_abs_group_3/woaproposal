package es.upm.emse.absd.ontology.woa;

import es.upm.emse.absd.ontology.woa.actions.*;
import es.upm.emse.absd.ontology.woa.concepts.*;
import static es.upm.emse.absd.ontology.woa.WoaOntologyVocabulary.*;
import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.schema.AgentActionSchema;
import jade.content.schema.ConceptSchema;
import jade.content.schema.ObjectSchema;
import jade.content.schema.TermSchema;
import lombok.extern.java.Log;


@Log
public class WoaOntology extends Ontology {
    // The singleton instance of this ontology
    private static final Ontology theInstance = new WoaOntology();
    public static Ontology getInstance() { return theInstance; }

    public WoaOntology() {
        super(ONTOLOGY_NAME, BasicOntology.getInstance());

        try {

            // adding AgentAction(s)
            AgentActionSchema informInitialResourcesSchema = new AgentActionSchema(INFORM_INITIAL_RESOURCES);
            add(informInitialResourcesSchema, InformInitialResources.class);

            AgentActionSchema registerSchema = new AgentActionSchema(REGISTER);
            add(registerSchema, Register.class);

            AgentActionSchema changePhaseSchema = new AgentActionSchema(CHANGE_PHASE);
            add(changePhaseSchema, ChangePhase.class);

            AgentActionSchema assignNewUnitSchema = new AgentActionSchema(ASSIGN_NEW_UNIT);
            add(assignNewUnitSchema, AssignNewUnit.class);

            AgentActionSchema allocateUnitSchema = new AgentActionSchema(ALLOCATE_UNIT);
            add(allocateUnitSchema, AllocateUnit.class);

            AgentActionSchema distributeMapSchema = new AgentActionSchema(DISTRIBUTE_MAP);
            add(distributeMapSchema, DistributeMap.class);

            // adding Predicate(s)


            // adding Concept(s)
            ConceptSchema resourceSchema = new ConceptSchema(RESOURCE);
            add(resourceSchema, Resource.class);

            ConceptSchema initialResourcesSchema = new ConceptSchema(INITIAL_RESOURCES);
            add(initialResourcesSchema, InitialResources.class);

            ConceptSchema storageCapacitySchema = new ConceptSchema(STORAGE_CAPACITY);
            add(storageCapacitySchema, StorageCapacity.class);

            ConceptSchema newPhaseSchema = new ConceptSchema(NEW_PHASE);
            add(newPhaseSchema, NewPhase.class);

            ConceptSchema coordinateSchema = new ConceptSchema(COORDINATE);
            add(coordinateSchema, Coordinate.class);



            // adding fields
            resourceSchema.add(RESOURCE_TYPE, (TermSchema)getSchema(BasicOntology.STRING), ObjectSchema.MANDATORY);
            resourceSchema.add(RESOURCE_AMOUNT, (TermSchema)getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);

            initialResourcesSchema.add(RESOURCE_GOLD, resourceSchema, ObjectSchema.MANDATORY);
            initialResourcesSchema.add(RESOURCE_STONE, resourceSchema, ObjectSchema.MANDATORY);
            initialResourcesSchema.add(RESOURCE_WOOD, resourceSchema, ObjectSchema.MANDATORY);

            storageCapacitySchema.add(STORAGE_SIZE, (TermSchema)getSchema(BasicOntology.FLOAT), ObjectSchema.MANDATORY);

            newPhaseSchema.add(PHASE_ID,(TermSchema)getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY);

            informInitialResourcesSchema.add(TRIBE_RESOURCES, initialResourcesSchema, ObjectSchema.MANDATORY);
            informInitialResourcesSchema.add(STORAGE, storageCapacitySchema, ObjectSchema.MANDATORY);

            coordinateSchema.add(COORDINATE_XVALUE, (TermSchema)getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY );
            coordinateSchema.add(COORDINATE_YVALUE, (TermSchema)getSchema(BasicOntology.INTEGER), ObjectSchema.MANDATORY );

            assignNewUnitSchema.add(AID_UNIT_ID, (TermSchema)getSchema(BasicOntology.AID), ObjectSchema.MANDATORY  );

            allocateUnitSchema.add(COORDINATE_INITIAL_POSITION, coordinateSchema, ObjectSchema.MANDATORY);

            distributeMapSchema.add(COORDINATE_MAP_SIZE, coordinateSchema, ObjectSchema.MANDATORY);
        }catch (java.lang.Exception e) {
            log.warning(e.getMessage());
        }
    }
}