package es.upm.emse.absd.ontology.woa.actions;

import jade.content.AgentAction;
import jade.core.AID;
import lombok.Data;

@Data
public class AssignNewUnit implements AgentAction {
    private AID unitID;
}