package es.upm.emse.absd.ontology.woa.actions;

import es.upm.emse.absd.ontology.woa.concepts.InitialResources;
import es.upm.emse.absd.ontology.woa.concepts.StorageCapacity;
import jade.content.AgentAction;
import lombok.Data;

@Data
public class InformInitialResources implements AgentAction {

    private InitialResources tribeResources;
    private StorageCapacity storage;
}
