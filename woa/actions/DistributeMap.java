package es.upm.emse.absd.ontology.woa.actions;

import es.upm.emse.absd.ontology.woa.concepts.Coordinate;
import jade.content.AgentAction;
import lombok.Data;

@Data
public class DistributeMap implements AgentAction {
    private Coordinate mapSize;
}